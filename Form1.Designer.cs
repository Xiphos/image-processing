﻿namespace ImageProcessing
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            this.btnBrowse = new System.Windows.Forms.Button();
            this.picImage = new System.Windows.Forms.PictureBox();
            this.btnNegative = new System.Windows.Forms.Button();
            this.gbBasic = new System.Windows.Forms.GroupBox();
            this.nudThreshold = new System.Windows.Forms.NumericUpDown();
            this.btnThreshold = new System.Windows.Forms.Button();
            this.btnHistogramStretch = new System.Windows.Forms.Button();
            this.gbTransform = new System.Windows.Forms.GroupBox();
            this.btnRotate180 = new System.Windows.Forms.Button();
            this.btnHorizontal = new System.Windows.Forms.Button();
            this.btnVerticalFlip = new System.Windows.Forms.Button();
            this.nudLighten = new System.Windows.Forms.NumericUpDown();
            this.btnLighten = new System.Windows.Forms.Button();
            this.nudDarken = new System.Windows.Forms.NumericUpDown();
            this.btnDarken = new System.Windows.Forms.Button();
            this.gbChannelExtract = new System.Windows.Forms.GroupBox();
            this.btnExtractRed = new System.Windows.Forms.Button();
            this.btnExtractBlue = new System.Windows.Forms.Button();
            this.btnExtractGreen = new System.Windows.Forms.Button();
            this.btnGrayscale = new System.Windows.Forms.Button();
            this.btnPolarize = new System.Windows.Forms.Button();
            this.btnSunset = new System.Windows.Forms.Button();
            this.btnReset = new System.Windows.Forms.Button();
            this.gbAdvanced = new System.Windows.Forms.GroupBox();
            this.btnHelp = new System.Windows.Forms.Button();
            this.cbExtraSharpen = new System.Windows.Forms.CheckBox();
            this.cbAutoThresh = new System.Windows.Forms.CheckBox();
            this.btnSobelDetect = new System.Windows.Forms.Button();
            this.nudThresh = new System.Windows.Forms.NumericUpDown();
            this.btnEdgeDetect = new System.Windows.Forms.Button();
            this.nudBlurLevel = new System.Windows.Forms.NumericUpDown();
            this.nudPixellate = new System.Windows.Forms.NumericUpDown();
            this.btnSort = new System.Windows.Forms.Button();
            this.btnBlur = new System.Windows.Forms.Button();
            this.btnPixellate = new System.Windows.Forms.Button();
            this.btnSwapCorners = new System.Windows.Forms.Button();
            this.btnUp = new System.Windows.Forms.Button();
            this.btnDown = new System.Windows.Forms.Button();
            this.btnRight = new System.Windows.Forms.Button();
            this.btnLeft = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.cHistogram = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.btnDevelop = new System.Windows.Forms.Button();
            this.gbHistogramChannels = new System.Windows.Forms.GroupBox();
            this.cbAverage = new System.Windows.Forms.CheckBox();
            this.cbGreen = new System.Windows.Forms.CheckBox();
            this.cbBlue = new System.Windows.Forms.CheckBox();
            this.cbRed = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.picImage)).BeginInit();
            this.gbBasic.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudThreshold)).BeginInit();
            this.gbTransform.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudLighten)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudDarken)).BeginInit();
            this.gbChannelExtract.SuspendLayout();
            this.gbAdvanced.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudThresh)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudBlurLevel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudPixellate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cHistogram)).BeginInit();
            this.gbHistogramChannels.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnBrowse
            // 
            this.btnBrowse.Location = new System.Drawing.Point(285, 289);
            this.btnBrowse.Name = "btnBrowse";
            this.btnBrowse.Size = new System.Drawing.Size(75, 23);
            this.btnBrowse.TabIndex = 1;
            this.btnBrowse.Text = "Browse";
            this.btnBrowse.UseVisualStyleBackColor = true;
            this.btnBrowse.Click += new System.EventHandler(this.btnBrowse_Click);
            // 
            // picImage
            // 
            this.picImage.Location = new System.Drawing.Point(40, 29);
            this.picImage.Name = "picImage";
            this.picImage.Size = new System.Drawing.Size(320, 240);
            this.picImage.TabIndex = 3;
            this.picImage.TabStop = false;
            // 
            // btnNegative
            // 
            this.btnNegative.Location = new System.Drawing.Point(16, 79);
            this.btnNegative.Name = "btnNegative";
            this.btnNegative.Size = new System.Drawing.Size(75, 23);
            this.btnNegative.TabIndex = 4;
            this.btnNegative.Text = "Negative";
            this.btnNegative.UseVisualStyleBackColor = true;
            this.btnNegative.Click += new System.EventHandler(this.btnNegative_Click);
            // 
            // gbBasic
            // 
            this.gbBasic.Controls.Add(this.nudThreshold);
            this.gbBasic.Controls.Add(this.btnThreshold);
            this.gbBasic.Controls.Add(this.btnHistogramStretch);
            this.gbBasic.Controls.Add(this.gbTransform);
            this.gbBasic.Controls.Add(this.nudLighten);
            this.gbBasic.Controls.Add(this.btnLighten);
            this.gbBasic.Controls.Add(this.nudDarken);
            this.gbBasic.Controls.Add(this.btnDarken);
            this.gbBasic.Controls.Add(this.gbChannelExtract);
            this.gbBasic.Controls.Add(this.btnGrayscale);
            this.gbBasic.Controls.Add(this.btnPolarize);
            this.gbBasic.Controls.Add(this.btnSunset);
            this.gbBasic.Controls.Add(this.btnNegative);
            this.gbBasic.Location = new System.Drawing.Point(379, 29);
            this.gbBasic.Name = "gbBasic";
            this.gbBasic.Size = new System.Drawing.Size(361, 293);
            this.gbBasic.TabIndex = 3;
            this.gbBasic.TabStop = false;
            this.gbBasic.Text = "Basic Effects";
            // 
            // nudThreshold
            // 
            this.nudThreshold.Location = new System.Drawing.Point(303, 51);
            this.nudThreshold.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.nudThreshold.Name = "nudThreshold";
            this.nudThreshold.Size = new System.Drawing.Size(39, 20);
            this.nudThreshold.TabIndex = 10;
            // 
            // btnThreshold
            // 
            this.btnThreshold.Location = new System.Drawing.Point(206, 50);
            this.btnThreshold.Name = "btnThreshold";
            this.btnThreshold.Size = new System.Drawing.Size(90, 23);
            this.btnThreshold.TabIndex = 9;
            this.btnThreshold.Text = "Threshold";
            this.btnThreshold.UseVisualStyleBackColor = true;
            this.btnThreshold.Click += new System.EventHandler(this.btnThreshold_Click);
            // 
            // btnHistogramStretch
            // 
            this.btnHistogramStretch.Location = new System.Drawing.Point(206, 19);
            this.btnHistogramStretch.Name = "btnHistogramStretch";
            this.btnHistogramStretch.Size = new System.Drawing.Size(136, 23);
            this.btnHistogramStretch.TabIndex = 8;
            this.btnHistogramStretch.Text = "Histogram Stretch";
            this.btnHistogramStretch.UseVisualStyleBackColor = true;
            this.btnHistogramStretch.Click += new System.EventHandler(this.btnHistogramStretch_Click);
            // 
            // gbTransform
            // 
            this.gbTransform.Controls.Add(this.btnRotate180);
            this.gbTransform.Controls.Add(this.btnHorizontal);
            this.gbTransform.Controls.Add(this.btnVerticalFlip);
            this.gbTransform.Location = new System.Drawing.Point(16, 198);
            this.gbTransform.Name = "gbTransform";
            this.gbTransform.Size = new System.Drawing.Size(175, 85);
            this.gbTransform.TabIndex = 13;
            this.gbTransform.TabStop = false;
            this.gbTransform.Text = "Transform";
            // 
            // btnRotate180
            // 
            this.btnRotate180.Location = new System.Drawing.Point(105, 19);
            this.btnRotate180.Name = "btnRotate180";
            this.btnRotate180.Size = new System.Drawing.Size(64, 52);
            this.btnRotate180.TabIndex = 2;
            this.btnRotate180.Text = "Rotate 180";
            this.btnRotate180.UseVisualStyleBackColor = true;
            this.btnRotate180.Click += new System.EventHandler(this.btnRotate180_Click);
            // 
            // btnHorizontal
            // 
            this.btnHorizontal.Location = new System.Drawing.Point(6, 48);
            this.btnHorizontal.Name = "btnHorizontal";
            this.btnHorizontal.Size = new System.Drawing.Size(92, 23);
            this.btnHorizontal.TabIndex = 1;
            this.btnHorizontal.Text = "Horizontal Flip";
            this.btnHorizontal.UseVisualStyleBackColor = true;
            this.btnHorizontal.Click += new System.EventHandler(this.btnHorizontal_Click);
            // 
            // btnVerticalFlip
            // 
            this.btnVerticalFlip.Location = new System.Drawing.Point(6, 19);
            this.btnVerticalFlip.Name = "btnVerticalFlip";
            this.btnVerticalFlip.Size = new System.Drawing.Size(92, 23);
            this.btnVerticalFlip.TabIndex = 0;
            this.btnVerticalFlip.Text = "Vertical Flip";
            this.btnVerticalFlip.UseVisualStyleBackColor = true;
            this.btnVerticalFlip.Click += new System.EventHandler(this.btnVerticalFlip_Click);
            // 
            // nudLighten
            // 
            this.nudLighten.Location = new System.Drawing.Point(98, 49);
            this.nudLighten.Name = "nudLighten";
            this.nudLighten.Size = new System.Drawing.Size(94, 20);
            this.nudLighten.TabIndex = 3;
            // 
            // btnLighten
            // 
            this.btnLighten.Location = new System.Drawing.Point(16, 48);
            this.btnLighten.Name = "btnLighten";
            this.btnLighten.Size = new System.Drawing.Size(75, 23);
            this.btnLighten.TabIndex = 2;
            this.btnLighten.Text = "Lighten";
            this.btnLighten.UseVisualStyleBackColor = true;
            this.btnLighten.Click += new System.EventHandler(this.btnLighten_Click);
            // 
            // nudDarken
            // 
            this.nudDarken.Location = new System.Drawing.Point(98, 22);
            this.nudDarken.Name = "nudDarken";
            this.nudDarken.Size = new System.Drawing.Size(94, 20);
            this.nudDarken.TabIndex = 1;
            // 
            // btnDarken
            // 
            this.btnDarken.Location = new System.Drawing.Point(16, 19);
            this.btnDarken.Name = "btnDarken";
            this.btnDarken.Size = new System.Drawing.Size(75, 23);
            this.btnDarken.TabIndex = 0;
            this.btnDarken.Text = "Darken";
            this.btnDarken.UseVisualStyleBackColor = true;
            this.btnDarken.Click += new System.EventHandler(this.btnDarken_Click);
            // 
            // gbChannelExtract
            // 
            this.gbChannelExtract.Controls.Add(this.btnExtractRed);
            this.gbChannelExtract.Controls.Add(this.btnExtractBlue);
            this.gbChannelExtract.Controls.Add(this.btnExtractGreen);
            this.gbChannelExtract.Location = new System.Drawing.Point(97, 75);
            this.gbChannelExtract.Name = "gbChannelExtract";
            this.gbChannelExtract.Size = new System.Drawing.Size(94, 117);
            this.gbChannelExtract.TabIndex = 12;
            this.gbChannelExtract.TabStop = false;
            this.gbChannelExtract.Text = "Extract";
            // 
            // btnExtractRed
            // 
            this.btnExtractRed.Location = new System.Drawing.Point(33, 25);
            this.btnExtractRed.Name = "btnExtractRed";
            this.btnExtractRed.Size = new System.Drawing.Size(25, 23);
            this.btnExtractRed.TabIndex = 0;
            this.btnExtractRed.Text = "R";
            this.btnExtractRed.UseVisualStyleBackColor = true;
            this.btnExtractRed.Click += new System.EventHandler(this.btnExtractRed_Click);
            // 
            // btnExtractBlue
            // 
            this.btnExtractBlue.Location = new System.Drawing.Point(33, 77);
            this.btnExtractBlue.Name = "btnExtractBlue";
            this.btnExtractBlue.Size = new System.Drawing.Size(25, 23);
            this.btnExtractBlue.TabIndex = 2;
            this.btnExtractBlue.Text = "B";
            this.btnExtractBlue.UseVisualStyleBackColor = true;
            this.btnExtractBlue.Click += new System.EventHandler(this.btnExtractBlue_Click);
            // 
            // btnExtractGreen
            // 
            this.btnExtractGreen.Location = new System.Drawing.Point(33, 51);
            this.btnExtractGreen.Name = "btnExtractGreen";
            this.btnExtractGreen.Size = new System.Drawing.Size(25, 23);
            this.btnExtractGreen.TabIndex = 1;
            this.btnExtractGreen.Text = "G";
            this.btnExtractGreen.UseVisualStyleBackColor = true;
            this.btnExtractGreen.Click += new System.EventHandler(this.btnExtractGreen_Click);
            // 
            // btnGrayscale
            // 
            this.btnGrayscale.Location = new System.Drawing.Point(16, 168);
            this.btnGrayscale.Name = "btnGrayscale";
            this.btnGrayscale.Size = new System.Drawing.Size(75, 23);
            this.btnGrayscale.TabIndex = 7;
            this.btnGrayscale.Text = "Grayscale";
            this.btnGrayscale.UseVisualStyleBackColor = true;
            this.btnGrayscale.Click += new System.EventHandler(this.btnGrayscale_Click);
            // 
            // btnPolarize
            // 
            this.btnPolarize.Location = new System.Drawing.Point(16, 138);
            this.btnPolarize.Name = "btnPolarize";
            this.btnPolarize.Size = new System.Drawing.Size(75, 23);
            this.btnPolarize.TabIndex = 6;
            this.btnPolarize.Text = "Polarize";
            this.btnPolarize.UseVisualStyleBackColor = true;
            this.btnPolarize.Click += new System.EventHandler(this.btnPolarize_Click);
            // 
            // btnSunset
            // 
            this.btnSunset.Location = new System.Drawing.Point(16, 108);
            this.btnSunset.Name = "btnSunset";
            this.btnSunset.Size = new System.Drawing.Size(75, 23);
            this.btnSunset.TabIndex = 5;
            this.btnSunset.Text = "Sunset";
            this.btnSunset.UseVisualStyleBackColor = true;
            this.btnSunset.Click += new System.EventHandler(this.btnSunset_Click);
            // 
            // btnReset
            // 
            this.btnReset.Location = new System.Drawing.Point(40, 289);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(75, 23);
            this.btnReset.TabIndex = 0;
            this.btnReset.Text = "Reset";
            this.btnReset.UseVisualStyleBackColor = true;
            this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
            // 
            // gbAdvanced
            // 
            this.gbAdvanced.Controls.Add(this.btnHelp);
            this.gbAdvanced.Controls.Add(this.cbExtraSharpen);
            this.gbAdvanced.Controls.Add(this.cbAutoThresh);
            this.gbAdvanced.Controls.Add(this.btnSobelDetect);
            this.gbAdvanced.Controls.Add(this.nudThresh);
            this.gbAdvanced.Controls.Add(this.btnEdgeDetect);
            this.gbAdvanced.Controls.Add(this.nudBlurLevel);
            this.gbAdvanced.Controls.Add(this.nudPixellate);
            this.gbAdvanced.Controls.Add(this.btnSort);
            this.gbAdvanced.Controls.Add(this.btnBlur);
            this.gbAdvanced.Controls.Add(this.btnPixellate);
            this.gbAdvanced.Controls.Add(this.btnSwapCorners);
            this.gbAdvanced.Location = new System.Drawing.Point(379, 328);
            this.gbAdvanced.Name = "gbAdvanced";
            this.gbAdvanced.Size = new System.Drawing.Size(361, 157);
            this.gbAdvanced.TabIndex = 4;
            this.gbAdvanced.TabStop = false;
            this.gbAdvanced.Text = "Advanced Effects";
            // 
            // btnHelp
            // 
            this.btnHelp.Location = new System.Drawing.Point(303, 98);
            this.btnHelp.Name = "btnHelp";
            this.btnHelp.Size = new System.Drawing.Size(31, 23);
            this.btnHelp.TabIndex = 11;
            this.btnHelp.Text = "?";
            this.btnHelp.UseVisualStyleBackColor = true;
            this.btnHelp.Click += new System.EventHandler(this.btnHelp_Click);
            // 
            // cbExtraSharpen
            // 
            this.cbExtraSharpen.AutoSize = true;
            this.cbExtraSharpen.Checked = true;
            this.cbExtraSharpen.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbExtraSharpen.Location = new System.Drawing.Point(198, 110);
            this.cbExtraSharpen.Name = "cbExtraSharpen";
            this.cbExtraSharpen.Size = new System.Drawing.Size(93, 17);
            this.cbExtraSharpen.TabIndex = 10;
            this.cbExtraSharpen.Text = "Extra Sharpen";
            this.cbExtraSharpen.UseVisualStyleBackColor = true;
            this.cbExtraSharpen.CheckedChanged += new System.EventHandler(this.cbExtraSharpen_CheckedChanged);
            // 
            // cbAutoThresh
            // 
            this.cbAutoThresh.AutoSize = true;
            this.cbAutoThresh.Location = new System.Drawing.Point(198, 88);
            this.cbAutoThresh.Name = "cbAutoThresh";
            this.cbAutoThresh.Size = new System.Drawing.Size(98, 17);
            this.cbAutoThresh.TabIndex = 9;
            this.cbAutoThresh.Text = "Auto Threshold";
            this.cbAutoThresh.UseVisualStyleBackColor = true;
            this.cbAutoThresh.CheckedChanged += new System.EventHandler(this.cbAutoThresh_CheckedChanged);
            // 
            // btnSobelDetect
            // 
            this.btnSobelDetect.Location = new System.Drawing.Point(196, 59);
            this.btnSobelDetect.Name = "btnSobelDetect";
            this.btnSobelDetect.Size = new System.Drawing.Size(138, 23);
            this.btnSobelDetect.TabIndex = 8;
            this.btnSobelDetect.Text = "Sobel Detect";
            this.btnSobelDetect.UseVisualStyleBackColor = true;
            this.btnSobelDetect.Click += new System.EventHandler(this.btnSobelDetect_Click);
            // 
            // nudThresh
            // 
            this.nudThresh.Location = new System.Drawing.Point(283, 29);
            this.nudThresh.Maximum = new decimal(new int[] {
            256,
            0,
            0,
            0});
            this.nudThresh.Name = "nudThresh";
            this.nudThresh.Size = new System.Drawing.Size(51, 20);
            this.nudThresh.TabIndex = 7;
            // 
            // btnEdgeDetect
            // 
            this.btnEdgeDetect.Location = new System.Drawing.Point(196, 29);
            this.btnEdgeDetect.Name = "btnEdgeDetect";
            this.btnEdgeDetect.Size = new System.Drawing.Size(81, 23);
            this.btnEdgeDetect.TabIndex = 6;
            this.btnEdgeDetect.Text = "Edge Detect";
            this.btnEdgeDetect.UseVisualStyleBackColor = true;
            this.btnEdgeDetect.Click += new System.EventHandler(this.btnEdgeDetect_Click);
            // 
            // nudBlurLevel
            // 
            this.nudBlurLevel.Location = new System.Drawing.Point(139, 88);
            this.nudBlurLevel.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudBlurLevel.Name = "nudBlurLevel";
            this.nudBlurLevel.Size = new System.Drawing.Size(32, 20);
            this.nudBlurLevel.TabIndex = 4;
            this.nudBlurLevel.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // nudPixellate
            // 
            this.nudPixellate.Increment = new decimal(new int[] {
            4,
            0,
            0,
            0});
            this.nudPixellate.Location = new System.Drawing.Point(139, 58);
            this.nudPixellate.Maximum = new decimal(new int[] {
            8,
            0,
            0,
            0});
            this.nudPixellate.Minimum = new decimal(new int[] {
            4,
            0,
            0,
            0});
            this.nudPixellate.Name = "nudPixellate";
            this.nudPixellate.Size = new System.Drawing.Size(32, 20);
            this.nudPixellate.TabIndex = 2;
            this.nudPixellate.Value = new decimal(new int[] {
            4,
            0,
            0,
            0});
            // 
            // btnSort
            // 
            this.btnSort.Location = new System.Drawing.Point(33, 116);
            this.btnSort.Name = "btnSort";
            this.btnSort.Size = new System.Drawing.Size(138, 23);
            this.btnSort.TabIndex = 5;
            this.btnSort.Text = "Sort";
            this.btnSort.UseVisualStyleBackColor = true;
            this.btnSort.Click += new System.EventHandler(this.btnSort_Click);
            // 
            // btnBlur
            // 
            this.btnBlur.Location = new System.Drawing.Point(33, 87);
            this.btnBlur.Name = "btnBlur";
            this.btnBlur.Size = new System.Drawing.Size(100, 23);
            this.btnBlur.TabIndex = 3;
            this.btnBlur.Text = "Blur";
            this.btnBlur.UseVisualStyleBackColor = true;
            this.btnBlur.Click += new System.EventHandler(this.btnBlur_Click);
            // 
            // btnPixellate
            // 
            this.btnPixellate.Location = new System.Drawing.Point(33, 58);
            this.btnPixellate.Name = "btnPixellate";
            this.btnPixellate.Size = new System.Drawing.Size(100, 23);
            this.btnPixellate.TabIndex = 1;
            this.btnPixellate.Text = "Pixellate";
            this.btnPixellate.UseVisualStyleBackColor = true;
            this.btnPixellate.Click += new System.EventHandler(this.btnPixellate_Click);
            // 
            // btnSwapCorners
            // 
            this.btnSwapCorners.Location = new System.Drawing.Point(33, 29);
            this.btnSwapCorners.Name = "btnSwapCorners";
            this.btnSwapCorners.Size = new System.Drawing.Size(138, 23);
            this.btnSwapCorners.TabIndex = 0;
            this.btnSwapCorners.Text = "Switch Corners";
            this.btnSwapCorners.UseVisualStyleBackColor = true;
            this.btnSwapCorners.Click += new System.EventHandler(this.btnSwapCorners_Click);
            // 
            // btnUp
            // 
            this.btnUp.Location = new System.Drawing.Point(182, 381);
            this.btnUp.Name = "btnUp";
            this.btnUp.Size = new System.Drawing.Size(25, 25);
            this.btnUp.TabIndex = 5;
            this.btnUp.Text = "↑";
            this.btnUp.UseVisualStyleBackColor = true;
            this.btnUp.Click += new System.EventHandler(this.btnUp_Click);
            // 
            // btnDown
            // 
            this.btnDown.Location = new System.Drawing.Point(182, 438);
            this.btnDown.Name = "btnDown";
            this.btnDown.Size = new System.Drawing.Size(25, 25);
            this.btnDown.TabIndex = 8;
            this.btnDown.Text = "↓";
            this.btnDown.UseVisualStyleBackColor = true;
            this.btnDown.Click += new System.EventHandler(this.btnDown_Click);
            // 
            // btnRight
            // 
            this.btnRight.Location = new System.Drawing.Point(214, 410);
            this.btnRight.Name = "btnRight";
            this.btnRight.Size = new System.Drawing.Size(25, 25);
            this.btnRight.TabIndex = 6;
            this.btnRight.Text = "→";
            this.btnRight.UseVisualStyleBackColor = true;
            this.btnRight.Click += new System.EventHandler(this.btnRight_Click);
            // 
            // btnLeft
            // 
            this.btnLeft.Location = new System.Drawing.Point(151, 410);
            this.btnLeft.Name = "btnLeft";
            this.btnLeft.Size = new System.Drawing.Size(25, 25);
            this.btnLeft.TabIndex = 7;
            this.btnLeft.Text = "← ";
            this.btnLeft.UseVisualStyleBackColor = true;
            this.btnLeft.Click += new System.EventHandler(this.btnLeft_Click);
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(285, 318);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 2;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // cHistogram
            // 
            chartArea1.AxisX.Maximum = 255D;
            chartArea1.AxisX.Minimum = 0D;
            chartArea1.AxisY.Maximum = 76800D;
            chartArea1.AxisY.Minimum = 0D;
            chartArea1.AxisY2.Enabled = System.Windows.Forms.DataVisualization.Charting.AxisEnabled.True;
            chartArea1.Name = "caHistogram";
            chartArea1.Position.Auto = false;
            chartArea1.Position.Height = 90F;
            chartArea1.Position.Width = 80F;
            chartArea1.Position.X = 5F;
            chartArea1.Position.Y = 10F;
            this.cHistogram.ChartAreas.Add(chartArea1);
            legend1.Name = "Legend1";
            this.cHistogram.Legends.Add(legend1);
            this.cHistogram.Location = new System.Drawing.Point(771, 80);
            this.cHistogram.Name = "cHistogram";
            this.cHistogram.Size = new System.Drawing.Size(376, 279);
            this.cHistogram.TabIndex = 9;
            this.cHistogram.Text = "Histogram";
            // 
            // btnDevelop
            // 
            this.btnDevelop.Location = new System.Drawing.Point(1041, 365);
            this.btnDevelop.Name = "btnDevelop";
            this.btnDevelop.Size = new System.Drawing.Size(106, 23);
            this.btnDevelop.TabIndex = 11;
            this.btnDevelop.Text = "Develop";
            this.btnDevelop.UseVisualStyleBackColor = true;
            this.btnDevelop.Click += new System.EventHandler(this.btnDevelop_Click);
            // 
            // gbHistogramChannels
            // 
            this.gbHistogramChannels.Controls.Add(this.cbAverage);
            this.gbHistogramChannels.Controls.Add(this.cbGreen);
            this.gbHistogramChannels.Controls.Add(this.cbBlue);
            this.gbHistogramChannels.Controls.Add(this.cbRed);
            this.gbHistogramChannels.Location = new System.Drawing.Point(771, 365);
            this.gbHistogramChannels.Name = "gbHistogramChannels";
            this.gbHistogramChannels.Size = new System.Drawing.Size(145, 116);
            this.gbHistogramChannels.TabIndex = 10;
            this.gbHistogramChannels.TabStop = false;
            this.gbHistogramChannels.Text = "Histogram Channels";
            // 
            // cbAverage
            // 
            this.cbAverage.AutoSize = true;
            this.cbAverage.Checked = true;
            this.cbAverage.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbAverage.Location = new System.Drawing.Point(23, 90);
            this.cbAverage.Name = "cbAverage";
            this.cbAverage.Size = new System.Drawing.Size(114, 17);
            this.cbAverage.TabIndex = 3;
            this.cbAverage.Text = "Average (Intensity)";
            this.cbAverage.UseVisualStyleBackColor = true;
            this.cbAverage.CheckedChanged += new System.EventHandler(this.cbAverage_CheckedChanged);
            // 
            // cbGreen
            // 
            this.cbGreen.AutoSize = true;
            this.cbGreen.Checked = true;
            this.cbGreen.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbGreen.Location = new System.Drawing.Point(23, 67);
            this.cbGreen.Name = "cbGreen";
            this.cbGreen.Size = new System.Drawing.Size(55, 17);
            this.cbGreen.TabIndex = 2;
            this.cbGreen.Text = "Green";
            this.cbGreen.UseVisualStyleBackColor = true;
            this.cbGreen.CheckedChanged += new System.EventHandler(this.cbGreen_CheckedChanged);
            // 
            // cbBlue
            // 
            this.cbBlue.AutoSize = true;
            this.cbBlue.Checked = true;
            this.cbBlue.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbBlue.Location = new System.Drawing.Point(23, 45);
            this.cbBlue.Name = "cbBlue";
            this.cbBlue.Size = new System.Drawing.Size(47, 17);
            this.cbBlue.TabIndex = 1;
            this.cbBlue.Text = "Blue";
            this.cbBlue.UseVisualStyleBackColor = true;
            this.cbBlue.CheckedChanged += new System.EventHandler(this.cbBlue_CheckedChanged);
            // 
            // cbRed
            // 
            this.cbRed.AutoSize = true;
            this.cbRed.Checked = true;
            this.cbRed.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbRed.Location = new System.Drawing.Point(23, 24);
            this.cbRed.Name = "cbRed";
            this.cbRed.Size = new System.Drawing.Size(46, 17);
            this.cbRed.TabIndex = 0;
            this.cbRed.Text = "Red";
            this.cbRed.UseVisualStyleBackColor = true;
            this.cbRed.CheckedChanged += new System.EventHandler(this.cbRed_CheckedChanged);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1159, 497);
            this.Controls.Add(this.gbHistogramChannels);
            this.Controls.Add(this.btnDevelop);
            this.Controls.Add(this.cHistogram);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.btnLeft);
            this.Controls.Add(this.btnRight);
            this.Controls.Add(this.btnDown);
            this.Controls.Add(this.btnUp);
            this.Controls.Add(this.gbAdvanced);
            this.Controls.Add(this.btnReset);
            this.Controls.Add(this.gbBasic);
            this.Controls.Add(this.btnBrowse);
            this.Controls.Add(this.picImage);
            this.MaximumSize = new System.Drawing.Size(1175, 536);
            this.MinimumSize = new System.Drawing.Size(1175, 536);
            this.Name = "Form1";
            this.Text = "Image Processing";
            ((System.ComponentModel.ISupportInitialize)(this.picImage)).EndInit();
            this.gbBasic.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.nudThreshold)).EndInit();
            this.gbTransform.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.nudLighten)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudDarken)).EndInit();
            this.gbChannelExtract.ResumeLayout(false);
            this.gbAdvanced.ResumeLayout(false);
            this.gbAdvanced.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudThresh)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudBlurLevel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudPixellate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cHistogram)).EndInit();
            this.gbHistogramChannels.ResumeLayout(false);
            this.gbHistogramChannels.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button btnBrowse;
        private System.Windows.Forms.PictureBox picImage;
        private System.Windows.Forms.Button btnNegative;
        private System.Windows.Forms.GroupBox gbBasic;
        private System.Windows.Forms.GroupBox gbTransform;
        private System.Windows.Forms.Button btnRotate180;
        private System.Windows.Forms.Button btnHorizontal;
        private System.Windows.Forms.Button btnVerticalFlip;
        private System.Windows.Forms.NumericUpDown nudLighten;
        private System.Windows.Forms.Button btnLighten;
        private System.Windows.Forms.NumericUpDown nudDarken;
        private System.Windows.Forms.Button btnDarken;
        private System.Windows.Forms.GroupBox gbChannelExtract;
        private System.Windows.Forms.Button btnExtractRed;
        private System.Windows.Forms.Button btnExtractBlue;
        private System.Windows.Forms.Button btnExtractGreen;
        private System.Windows.Forms.Button btnGrayscale;
        private System.Windows.Forms.Button btnPolarize;
        private System.Windows.Forms.Button btnSunset;
        private System.Windows.Forms.Button btnReset;
        private System.Windows.Forms.GroupBox gbAdvanced;
        private System.Windows.Forms.Button btnSwapCorners;
        private System.Windows.Forms.Button btnSort;
        private System.Windows.Forms.Button btnBlur;
        private System.Windows.Forms.Button btnPixellate;
        private System.Windows.Forms.NumericUpDown nudPixellate;
        private System.Windows.Forms.NumericUpDown nudBlurLevel;
        private System.Windows.Forms.Button btnUp;
        private System.Windows.Forms.Button btnDown;
        private System.Windows.Forms.Button btnRight;
        private System.Windows.Forms.Button btnLeft;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnEdgeDetect;
        private System.Windows.Forms.NumericUpDown nudThresh;
        private System.Windows.Forms.Button btnSobelDetect;
        private System.Windows.Forms.CheckBox cbAutoThresh;
        private System.Windows.Forms.Button btnHistogramStretch;
        private System.Windows.Forms.CheckBox cbExtraSharpen;
        private System.Windows.Forms.Button btnThreshold;
        private System.Windows.Forms.DataVisualization.Charting.Chart cHistogram;
        private System.Windows.Forms.NumericUpDown nudThreshold;
        private System.Windows.Forms.Button btnDevelop;
        private System.Windows.Forms.GroupBox gbHistogramChannels;
        private System.Windows.Forms.CheckBox cbAverage;
        private System.Windows.Forms.CheckBox cbGreen;
        private System.Windows.Forms.CheckBox cbBlue;
        private System.Windows.Forms.CheckBox cbRed;
        private System.Windows.Forms.Button btnHelp;
    }
}

