﻿using System;
using System.Drawing;
using System.Windows.Forms;
using System.IO;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;
using System.Data;
using System.Diagnostics;

namespace ImageProcessing
{
    public partial class Form1 : Form
    {
        //Variables
        Bitmap bitmapImage;
        Bitmap originalImage; //for reset button
        Color[,] ImageArray = new Color[320, 240];
        int imWidth = 320;
        int imHeight = 240;
        bool extraSharpen = false;

        //Enum for the cumulative distribution function
        enum CDF
        {
            RedCDF,
            GreenCDF,
            BlueCDF,
            AverageCDF,
        };

        public Form1()
        {
            InitializeComponent();

            //Initialize the bitmap, image, and array
            bitmapImage = loadBlank();
            SetArrayFromBitmap();
            picImage.Image = bitmapImage;
            originalImage = bitmapImage;

            developHistogram();
        }
        public Bitmap loadBlank()
        {
            //Returns a blank white bitmap

            Bitmap bmp = new Bitmap(320, 240);
            using (Graphics graph = Graphics.FromImage(bmp))
            {
                Rectangle ImageSize = new Rectangle(0, 0, 320, 240);
                graph.FillRectangle(Brushes.White, ImageSize);
            }
            return bmp;
        }

        //Colormatrix experiments
        private static Bitmap ApplyColorMatrix(Image src, ColorMatrix cMat)
        {
            Bitmap bmpSrc = GetArgbCopy(src);
            Bitmap bmpOut = new Bitmap(bmpSrc.Width, bmpSrc.Height, PixelFormat.Format32bppArgb);

            using (Graphics g = Graphics.FromImage(bmpOut))
            {
                ImageAttributes bmpAttrib = new ImageAttributes();
                bmpAttrib.SetColorMatrix(cMat);
                Rectangle rct = new Rectangle(0, 0, bmpSrc.Width, bmpSrc.Height);
                g.DrawImage(bmpSrc, rct, 0, 0, bmpSrc.Width, bmpSrc.Height, GraphicsUnit.Pixel, bmpAttrib);
            }

            bmpSrc.Dispose();
            return bmpOut;
        }
        private static Bitmap GetArgbCopy(Image sourceImage)
        {
            Bitmap bmpNew = new Bitmap(sourceImage.Width, sourceImage.Height, PixelFormat.Format32bppArgb);

            using (Graphics graphics = Graphics.FromImage(bmpNew))
            {
                graphics.DrawImage(sourceImage, new Rectangle(0, 0, bmpNew.Width, bmpNew.Height),
            new Rectangle(0, 0, bmpNew.Width, bmpNew.Height), GraphicsUnit.Pixel);
                graphics.Flush();
            }
            return bmpNew;
        }
        //Gaussian blur
        public void Gauss(Image src)
        {
            Bitmap bmp = new Bitmap(src);
            Color[,] blurredArray = new Color[320, 240];

            //3x3 gaussian matrix
            float[][] gaussMat = new float[][]
            {
                new float[]{0.077847f, 0.123317f, 0.077847f},
                new float[]{0.123317f, 0.195346f, 0.123317f},
                new float[]{ 0.077847f, 0.123317f, 0.077847f }
            };

            //Loop through the image
            for (int y = 0; y < bmp.Height; y++)
            {
                for (int x = 0; x < bmp.Width; x++)
                {
                    //Accumulators and total
                    int accumR = 0;
                    int accumG = 0;
                    int accumB = 0;
                    int pixel = 0;

                    for (int mY = 0; mY < 3; mY++)
                    {
                        for (int mX = 0; mX < 3; mX++)
                        {
                            //Get the coordinates of the source image
                            int sX = (x - 2) + mX;
                            int sY = (y - 2) + mY;

                            Color cCol;

                            //Edge handling (effectively reflect the inside edge to the outside)
                            if (sX < 0)
                            {
                                sX *= -1;
                            }
                            if (sY < 0)
                            {
                                sY *= -1;
                            }
                            if (sY >= (bmp.Height - 1))
                            {
                                sY = 2 * (bmp.Height - 1) - sY;
                            }
                            if (sX >= (bmp.Width - 1))
                            {
                                sX = 2 * (bmp.Width - 1) - sX;
                            }

                            //Modify the color at the current pixel by the current matrix value 
                            //and add it to the total
                            cCol = ImageArray[sX, sY];
                            accumR += (int)(cCol.R * gaussMat[mX][mY]);
                            accumG += (int)(cCol.G * gaussMat[mX][mY]);
                            accumB += (int)(cCol.B * gaussMat[mX][mY]);

                            pixel++;
                        }
                    }
                    //Make sure colors are within bounds
                    accumR = colBound(accumR);
                    accumG = colBound(accumG);
                    accumB = colBound(accumB);

                    //Handle thresholding (make edges more defined etc.)
                    /*int thresh = (int)nudBlurLevel.Value;
                    if (accumR >= thresh)
                    {
                        accumR = 255;
                    }
                    if(accumG >= thresh)
                    {
                        accumG = 255;
                    }
                    if(accumB >= thresh)
                    {
                        accumB = 255;
                    }*/

                    //Set the new color
                    Color avColor = (Color.FromArgb(255, accumR, accumG, accumB));
                    blurredArray[x, y] = avColor;

                }
            }

            ImageArray = blurredArray;
            SetBitmapFromArray();
            picImage.Image = bitmapImage;


        }
        //Sobel edge detection (easy edge detector)
        public void Sobel(Image src)
        {
            //Blur a bit and grayscale the image
            btnGrayscale.PerformClick();
            btnBlur.PerformClick();

            Bitmap bmp = new Bitmap(src);
            Color[,] edgedArray = new Color[320, 240];

            //Horizontal and vertical sobel kernels
            float[][] sobelX = new float[][]
            {
                new float[]{-1, 0, 1},
                new float[]{-2, 0, 2},
                new float[]{-1, 0, 1}
            };
            float[][] sobelY = new float[][]
            {
                new float[]{1, 2, 1},
                new float[]{0,0,0},
                new float[]{-1,-2,-1}
            };

            for (int y = 0; y < bmp.Height; y++)
            {
                for (int x = 0; x < bmp.Width; x++)
                {
                    //Accumulators and totals
                    int accumX = 0;
                    int accumY = 0;
                    int pixel = 0;

                    for (int mY = 0; mY < 3; mY++)
                    {
                        for (int mX = 0; mX < 3; mX++)
                        {
                            //Get the coordinates at the source image
                            int sX = (x - 2) + mX;
                            int sY = (y - 2) + mY;

                            Color cCol;

                            //Edge handling (reflect the inside edge over)
                            if (sX < 0)
                            {
                                sX *= -1;
                            }
                            if (sY < 0)
                            {
                                sY *= -1;
                            }
                            if (sY >= (bmp.Height - 1))
                            {
                                sY = 2 * (bmp.Height - 1) - sY;
                            }
                            if (sX >= (bmp.Width - 1))
                            {
                                sX = 2 * (bmp.Width - 1) - sX;
                            }

                            cCol = ImageArray[sX, sY];

                            //Increment the horizontal and vertical accumulators (after modifying by sobel kernel)
                            accumX += cCol.R * (int)sobelX[mX][mY];
                            accumY += cCol.R * (int)sobelY[mX][mY];
                            pixel++;

                        }
                    }

                    //Set the new color
                    int Intensity = colBound((int)Math.Sqrt((accumX * accumX) + (accumY * accumY)));
                    Color avColor = (Color.FromArgb(255, Intensity, Intensity, Intensity));
                    edgedArray[x, y] = avColor;

                }
            }

            //Replace the image array and display
            ImageArray = edgedArray;
            SetBitmapFromArray();
            picImage.Image = bitmapImage;
        }
        public int colBound(int x)
        {
            //Returns the integer within color boundaries
            return Math.Max(0, Math.Min(255, x));
        }

        //Edge Detection
        private struct Vector
        {
            //Vector structure for the gradient

            public int X;
            public int Y;

            public Vector(int x, int y)
            {
                this.X = x;
                this.Y = y;
            }
            public static Vector operator +(Vector a, Vector b)
            {
                Vector c = new Vector(a.X + b.X, a.Y + b.Y);
                return c;
            }
            public static Vector operator -(Vector a, Vector b)
            {
                return new Vector(a.X - b.X, a.Y - b.Y);
            }
            public static bool operator ==(Vector a, Vector b)
            {
                if (a.X == b.X && a.Y == b.Y)
                {
                    return true;
                }
                return false;
            }
            public static bool operator !=(Vector a, Vector b)
            {
                if (a.X == b.X && a.Y == b.Y)
                {
                    return false;
                }
                return true;
            }
            public override int GetHashCode()
            {
                return base.GetHashCode();
            }
            public override bool Equals(object obj)
            {
                return base.Equals(obj);
            }
        }
        private struct EdgePixel
        {
            public int X;
            public int Y;
            public Vector gradient;
            public string edgeType;
            public int magnitude;
            public int theta;
        }
        public int Otsu()
        {
            // Otsu method to find the best thresholding value for the image
            // Uses a statistical model to find a thresholding value where there is a maximum of inter-class variance
            // Maximizing the inter-class variance likewise minimizes the intra-class variance
            // The 'classes' are the background (dark colors) and foreground (light colors)
            btnGrayscale.PerformClick();
            SetArrayFromBitmap();

            int[] histogram = new int[256];

            int pixels = 320 * 240;

            //Develop the histogram
            for (int i = 0; i < 255; i++)
            {
                histogram[i] = 0;
            }
            for (int x = 0; x < 320; x++)
            {
                for (int y = 0; y < 240; y++)
                {
                    int cCol = ImageArray[x, y].R;
                    histogram[cCol] += 1;
                }
            }

            float sum = 0;
            for (int t = 0; t < 256; t++)
            {
                sum += t * histogram[t];
            }
            float sumB = 0;
            int wB = 0;
            int wF = 0;

            float varMax = 0;
            int threshold = 0;
            for (int t = 0; t < 256; t++)
            {
                wB += histogram[t];

                if (wB == 0)
                    continue;
                wF = pixels - wB;
                if (wF == 0)
                    break;
                sumB += (float)(t * histogram[t]);

                float mB = sumB / wB;
                float mF = (sum - sumB) / wF;

                float varBet = (float)wB * (float)wF * (mB - mF) * (mB - mF);


                //If the interclass variance is greater than the current max, make it the new max
                if (varBet > varMax)
                {
                    varMax = varBet;
                    threshold = t;
                }
            }
            nudThresh.Value = threshold;
            return threshold;

        }
        public void gradientDetect(Image src, int threshold)
        {
            //Edge detection algorithm based on the gradient of the image. 
            //Works best on images with bimodal histograms (use the threshold/histogram to optimize this)
            //Summary follows:
            // 1. Treat the image as a function of x and y (output is intensity)
            // 2. Pretend that it is a continuous function that is only sampled on the region [0,320]x[0,240]
            // 3. Calculate the gradient vector at each coorinate pair (where I(x,y) is the intensity function):
            //    ∂f/∂x = [I(x+1, y) - I(x-1, y)] ÷ 2
            //    ∂f/∂y = [I(x, y+1) - I(x, y-1)] ÷ 2
            // 4. This gradient gives the direction of greatest ascent of the hypothetical function
            // 5. A large magnitude means a large change in the function over the interval
            // 6. If that magnitude is greater than the threshold, make it intense
            //Also implements the Canny edge detection algorithm to thin edges and remove noise:
            // 1. Go through each pixel in the array
            // 2. Look at the pixels in the positive and negative gradient directions (rounded to one of the 8 adjacent pixels)
            // 3. These pixels are perpendicular to the 'edge', see if they are greater in magnitude than the current pixel
            // 4. If the current pixel is the greatest in magnitude, magnify it (otherwise suppress it)
            // 5. Go through all the weak pixels, and loop over the surrounding 8 pixels
            // 6. If there are at least 2 strong pixels in this blob, leave the pixel as it is (otherwise suppress)
            //Also implements the Otsu method to find optimal thresholding values (see Otsu() for details)

            //Grayscale and blur
            nudBlurLevel.Value = 1;
            btnBlur.PerformClick();

            //Arrays (the image, the gradient, and the magnitude of the gradient)
            int[,] image = new int[320, 240];
            EdgePixel[,] edges = new EdgePixel[320, 240];
            int upperThresh = threshold;
            int lowerThresh = upperThresh / 2;

            //Make sure the array is right
            SetArrayFromBitmap();

            //Set the image array based on the intensities (grayscale)
            for (int x = 0; x < 320; x++)
            {
                for (int y = 0; y < 240; y++)
                {
                    image[x, y] = ImageArray[x, y].R;
                    edges[x, y] = new EdgePixel();
                    edges[x, y].X = x;
                    edges[x, y].Y = y;
                }
            }

            //Get the gradient (ignore 1 pixel of image edge)
            for (int x = 1; x < 319; x++)
            {
                for (int y = 1; y < 239; y++)
                {
                    //Change in x = (f(x+1,y) - f(x-1,y))/2
                    //Similar for y
                    int dX = (image[x + 1, y] - image[x - 1, y]) / 2;
                    int dY = (image[x, y + 1] - image[x, y - 1]) / 2;

                    //Get the direction number (starts at 0 to the right, increases anti-clockwise)
                    double theta = Math.Atan2(dY, dX);
                    if (theta < 0)
                    {
                        theta += Math.PI;
                    }
                    theta = Math.Round(theta / (Math.PI / 4));

                    //Set the gradient,magnitude, and direction
                    edges[x, y].gradient = new Vector(dX, dY);
                    edges[x, y].magnitude = (int)Math.Sqrt(dX * dX + dY * dY);
                    edges[x, y].theta = (int)theta;
                    edges[x, y].edgeType = "NONE";

                    //Set pixel strength
                    if (edges[x, y].magnitude > lowerThresh)
                    {
                        edges[x, y].edgeType = "WEAK";
                        if (edges[x, y].magnitude > upperThresh)
                        {
                            edges[x, y].edgeType = "STRONG";
                        }
                    }

                }
            }

            for (int x = 1; x < 319; x++)
            {
                for (int y = 1; y < 239; y++)
                {
                    //Variables
                    EdgePixel cPix = edges[x, y];
                    EdgePixel adj1 = new EdgePixel();
                    EdgePixel adj2 = new EdgePixel();
                    int gradDir = cPix.theta;

                    //Find the pixels along the positive and negative gradient directions
                    if (gradDir == 0 || gradDir == 4)
                    {
                        //Left and Right
                        adj1 = edges[x - 1, y];
                        adj2 = edges[x + 1, y];
                    }
                    else if (gradDir == 1 || gradDir == 5)
                    {
                        //Up right and Down left
                        adj1 = edges[x + 1, y - 1];
                        adj2 = edges[x - 1, y + 1];
                    }
                    else if (gradDir == 2 || gradDir == 6)
                    {
                        //Up and Down
                        adj1 = edges[x, y + 1];
                        adj2 = edges[x, y - 1];
                    }
                    else if (gradDir == 3 || gradDir == 7)
                    {
                        //Up left and Down right
                        adj1 = edges[x - 1, y - 1];
                        adj2 = edges[x + 1, y + 1];
                    }

                    //Now see if the current pixel is the maximum value of the adjacents
                    if (!(cPix.magnitude > adj1.magnitude && cPix.magnitude > adj2.magnitude))
                    {
                        //If it is not the greatest, suppress
                        edges[x, y].magnitude /= 3;
                    }
                    else
                    {
                        //If it is the greatest, increase it a bit
                        if (extraSharpen)
                            edges[x, y].magnitude *= 8;
                    }

                }
            }

            //Double threshold pass
            for (int x = 1; x < 319; x++)
            {
                for (int y = 1; y < 239; y++)
                {
                    EdgePixel cPix = edges[x, y];


                    //Iterate over the blob and look for strong pixels
                    if (cPix.edgeType != "STRONG")
                    {
                        int strongInBlob = 0;
                        for (int Px = x - 1; Px < x + 1; Px++)
                        {
                            for (int Py = y - 1; Py < y + 1; Py++)
                            {
                                if (edges[x, y].edgeType == "STRONG")
                                {
                                    strongInBlob++;
                                }
                            }
                        }
                        if (strongInBlob < 1)
                        {
                            edges[x, y].magnitude /= 2;
                        }
                    }

                    //Handle how bright edges show as
                    if (edges[x, y].magnitude >= upperThresh)
                    {
                        edges[x, y].magnitude = colBound((int)(edges[x, y].magnitude * 12));
                    }

                    int col = colBound(edges[x, y].magnitude);
                    ImageArray[x, y] = Color.FromArgb(255, col, col, col);
                }
            }

            //Display image
            SetBitmapFromArray();
            picImage.Image = bitmapImage;
        }
        private void btnEdgeDetect_Click(object sender, EventArgs e)
        {
            Gauss(picImage.Image);
            //Use custom gradient edge detection
            //If the user wants, the otsu method will automatically determine the best thresholding value
            if (nudThresh.Value == 0 || cbAutoThresh.Checked)
            {
                int thresh = Otsu();
                gradientDetect(picImage.Image, thresh);

            }
            else
            {
                btnGrayscale.PerformClick();
                gradientDetect(picImage.Image, (int)nudThresh.Value);
            }
        }
        private void cbAutoThresh_CheckedChanged(object sender, EventArgs e)
        {
            if (cbAutoThresh.Checked)
            {
                nudThresh.Value = 0;
                nudThresh.Enabled = false;
            }
            else
            {
                nudThresh.Enabled = true;
            }
        }

        //Built-in methods
        private void btnBrowse_Click(object sender, EventArgs e)
        {
            Stream myStream = null;
            OpenFileDialog openFileDialog1 = new OpenFileDialog();

            //   openFileDialog1.InitialDirectory = System.Environment.GetFolderPath(System.Environment.SpecialFolder.MyPictures);
            openFileDialog1.Filter = "Images (*.BMP;*.JPG;*.GIF)|*.BMP;*.JPG;*.GIF|All files (*.*)|*.*";
            openFileDialog1.FilterIndex = 2;
            openFileDialog1.RestoreDirectory = true;
            openFileDialog1.Title = "Image Browser";


            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                if ((myStream = openFileDialog1.OpenFile()) != null)
                {
                    Image newImage = Image.FromStream(myStream);
                    bitmapImage = new Bitmap(newImage, 320, 240);
                    originalImage = new Bitmap(newImage, 320, 240);
                    picImage.Image = bitmapImage;
                    myStream.Close();
                }
            }

            SetArrayFromBitmap();

        }
        private void SetBitmapFromArray()
        {
            for (int row = 0; row < 320; row++)
                for (int col = 0; col < 240; col++)
                {
                    bitmapImage.SetPixel(row, col, ImageArray[row, col]);
                }
            developHistogram();
        }
        private void SetArrayFromBitmap()
        {
            for (int row = 0; row < 320; row++)
                for (int col = 0; col < 240; col++)
                {
                    ImageArray[row, col] = bitmapImage.GetPixel(row, col);
                }
            developHistogram();
        }

        //Other filters
        private void btnReset_Click(object sender, EventArgs e)
        {
            Bitmap temp = new Bitmap(originalImage);
            bitmapImage = temp;
            picImage.Image = bitmapImage;
            SetArrayFromBitmap();
        }
        private void btnExtractRed_Click(object sender, EventArgs e)
        {
            //Just get the red values (straightforward)


            if (bitmapImage == null)
                return;

            for (int x = 0; x < imWidth; x++)
            {
                for (int y = 0; y < imHeight; y++)
                {
                    Color curPixel = ImageArray[x, y];
                    Color newPixel = Color.FromArgb(255, curPixel.R, 0, 0);
                    ImageArray[x, y] = newPixel;
                }
            }

            SetBitmapFromArray();
            picImage.Image = bitmapImage;
        }
        private void btnExtractGreen_Click(object sender, EventArgs e)
        {
            //Just get the green values (straightforward)
            if (bitmapImage == null)
                return;

            for (int x = 0; x < imWidth; x++)
            {
                for (int y = 0; y < imHeight; y++)
                {
                    Color curPixel = ImageArray[x, y];
                    Color newPixel = Color.FromArgb(255, 0, curPixel.G, 0);
                    ImageArray[x, y] = newPixel;
                }
            }

            SetBitmapFromArray();
            picImage.Image = bitmapImage;
        }
        private void btnExtractBlue_Click(object sender, EventArgs e)
        {
            //Just get the blue values (straightforward)
            if (bitmapImage == null)
                return;

            for (int x = 0; x < imWidth; x++)
            {
                for (int y = 0; y < imHeight; y++)
                {
                    Color curPixel = ImageArray[x, y];
                    Color newPixel = Color.FromArgb(255, 0, 0, curPixel.B);
                    ImageArray[x, y] = newPixel;
                }
            }

            SetBitmapFromArray();
            picImage.Image = bitmapImage;
        }
        private void btnDarken_Click(object sender, EventArgs e)
        {
            //Multiply all the rgb values down by a decimal <1
            nudLighten.Value = 0;

            //Percent to darken by
            double percentDarken = 1 - ((double)nudDarken.Value / 100.0);

            for (int x = 0; x < imWidth; x++)
            {
                for (int y = 0; y < imHeight; y++)
                {
                    Color curPixel = ImageArray[x, y];
                    int nuRed = colBound((int)(curPixel.R * percentDarken));
                    int nuGreen = colBound((int)(curPixel.G * percentDarken));
                    int nuBlue = colBound((int)(curPixel.B * percentDarken));
                    Color nuPixel = Color.FromArgb(255, nuRed, nuGreen, nuBlue);

                    ImageArray[x, y] = nuPixel;
                }
            }

            SetBitmapFromArray();
            picImage.Image = bitmapImage;
        }
        private void btnLighten_Click(object sender, EventArgs e)
        {
            //Multiplay all the rgb values up by a decimal >1
            nudDarken.Value = 0;

            //Percent to lighten by
            double percentLighten = 1 + ((double)nudLighten.Value / 100.0);

            for (int x = 0; x < imWidth; x++)
            {
                for (int y = 0; y < imHeight; y++)
                {
                    Color curPixel = ImageArray[x, y];
                    int nuRed = colBound((int)(curPixel.R * percentLighten));
                    int nuGreen = colBound((int)(curPixel.G * percentLighten));
                    int nuBlue = colBound((int)(curPixel.B * percentLighten));

                    Color nuPixel = Color.FromArgb(255, nuRed, nuGreen, nuBlue);

                    ImageArray[x, y] = nuPixel;
                }
            }

            SetBitmapFromArray();
            picImage.Image = bitmapImage;
        }
        private void btnNegative_Click(object sender, EventArgs e)
        {
            //Subtract the r g b values from 255
            for (int x = 0; x < imWidth; x++)
            {
                for (int y = 0; y < imHeight; y++)
                {
                    Color curPixel = ImageArray[x, y];
                    int nuRed = 255 - curPixel.R;
                    int nuGreen = 255 - curPixel.G;
                    int nuBlue = 255 - curPixel.B;

                    Color nuPixel = Color.FromArgb(255, nuRed, nuGreen, nuBlue);

                    ImageArray[x, y] = nuPixel;
                }
            }

            SetBitmapFromArray();
            picImage.Image = bitmapImage;
        }
        private void btnPolarize_Click(object sender, EventArgs e)
        {
            //Calculate average rgb values
            int iRedTot = 0;
            int iGreenTot = 0;
            int iBlueTot = 0;
            int pixels = imWidth * imHeight;
            for (int x = 0; x < imWidth; x++)
            {
                for (int y = 0; y < imHeight; y++)
                {
                    Color curPixel = ImageArray[x, y];

                    iRedTot += curPixel.R;
                    iGreenTot += curPixel.G;
                    iBlueTot += curPixel.B;
                }
            }

            int iRAvg = iRedTot / pixels;
            int iGAvg = iGreenTot / pixels;
            int iBAvg = iBlueTot / pixels;

            //Polarize (if it is above the avg, set to 255, else set to 0)
            for (int x = 0; x < imWidth; x++)
            {
                for (int y = 0; y < imHeight; y++)
                {
                    Color curPixel = ImageArray[x, y];
                    int nuRed = 0;
                    int nuGreen = 0;
                    int nuBlue = 0;

                    if (curPixel.R >= iRAvg)
                    {
                        nuRed = 255;
                    }
                    if (curPixel.G >= iGAvg)
                    {
                        nuGreen = 255;
                    }
                    if (curPixel.B >= iBAvg)
                    {
                        nuBlue = 255;
                    }

                    Color nuPixel = Color.FromArgb(255, nuRed, nuGreen, nuBlue);

                    ImageArray[x, y] = nuPixel;
                }
            }
            //Display the image
            SetBitmapFromArray();
            picImage.Image = bitmapImage;
        }
        private void btnGrayscale_Click(object sender, EventArgs e)
        {
            //Set the rgb values to the average of them
            for (int x = 0; x < imWidth; x++)
            {
                for (int y = 0; y < imHeight; y++)
                {
                    Color curPixel = ImageArray[x, y];
                    int nuGray = (curPixel.R + curPixel.G + curPixel.B) / 3;

                    Color nuPixel = Color.FromArgb(255, nuGray, nuGray, nuGray);

                    ImageArray[x, y] = nuPixel;
                }
            }

            SetBitmapFromArray();
            picImage.Image = bitmapImage;
        }
        private void btnSunset_Click(object sender, EventArgs e)
        {
            //Scale up the red a bit and tone down the green/blue
            double redFactor = 1.1;
            double GBFactor = 0.9;
            for (int x = 0; x < imWidth; x++)
            {
                for (int y = 0; y < imHeight; y++)
                {
                    Color curPixel = ImageArray[x, y];
                    int nuRed = Math.Min(255, (int)(curPixel.R * redFactor));
                    int nuGreen = (int)(curPixel.G * GBFactor);
                    int nuBlue = (int)(curPixel.B * GBFactor);

                    Color nuPixel = Color.FromArgb(255, nuRed, nuGreen, nuBlue);

                    ImageArray[x, y] = nuPixel;
                }
            }

            SetBitmapFromArray();
            picImage.Image = bitmapImage;
        }
        private void btnVerticalFlip_Click(object sender, EventArgs e)
        {
            //Swap pixels across the horizontal axis using a temp pixel
            for (int y = 0; y < imHeight / 2; y++)
            {
                for (int x = 0; x < imWidth; x++)
                {
                    Color temp = ImageArray[x, y];
                    int nuY = (imHeight - 1) - y;
                    ImageArray[x, y] = ImageArray[x, nuY];
                    ImageArray[x, nuY] = temp;
                }
            }

            SetBitmapFromArray();
            picImage.Image = bitmapImage;
        }
        private void btnHorizontal_Click(object sender, EventArgs e)
        {
            //Flip the array over the vertical axis using a temporary pixel
            for (int x = 0; x < imWidth / 2; x++)
            {
                for (int y = 0; y < imHeight; y++)
                {
                    Color temp = ImageArray[x, y];
                    ImageArray[x, y] = ImageArray[(imWidth - 1) - x, y];
                    ImageArray[(imWidth - 1) - x, y] = temp;
                }
            }

            SetBitmapFromArray();
            picImage.Image = bitmapImage;
        }
        private void btnRotate180_Click(object sender, EventArgs e)
        {
            //Flip the array over the vertical axis using a temporary pixel
            for (int x = 0; x < imWidth / 2; x++)
            {
                for (int y = 0; y < imHeight; y++)
                {
                    Color temp = ImageArray[x, y];
                    ImageArray[x, y] = ImageArray[(imWidth - 1) - x, y];
                    ImageArray[(imWidth - 1) - x, y] = temp;
                }
            }
            //Swap pixels across the horizontal axis using a temp pixel
            for (int y = 0; y < imHeight / 2; y++)
            {
                for (int x = 0; x < imWidth; x++)
                {
                    Color temp = ImageArray[x, y];
                    int nuY = (imHeight - 1) - y;
                    ImageArray[x, y] = ImageArray[x, nuY];
                    ImageArray[x, nuY] = temp;
                }
            }

            SetBitmapFromArray();
            picImage.Image = bitmapImage;
        }
        private void btnSwapCorners_Click(object sender, EventArgs e)
        {
            //Arrays to select the top left and bottom right corners
            Color[,] topLeft = new Color[160, 120];
            int tlX = 0;
            int tlY = 0;
            Color[,] botRight = new Color[160, 120];
            int brX = 0;
            int brY = 0;

            //Select them
            for (int x = 0; x < imWidth; x++)
            {
                tlY = 0;
                brY = 0;
                for (int y = 0; y < imHeight; y++)
                {
                    Color curPixel = ImageArray[x, y];
                    if (x < 160 && y < 120)
                    {
                        topLeft[tlX, tlY] = curPixel;
                    }
                    if (x >= 160 && y >= 120)
                    {
                        botRight[brX, brY] = curPixel;
                    }
                    if (y < 120)
                    {
                        tlY++;
                    }
                    if (y >= 120)
                    {
                        brY++;
                    }
                }
                if (x < 160)
                {
                    tlX++;
                }
                if (x >= 160)
                {
                    brX++;
                }
            }
            //Set them
            for (int x = 0; x < 160; x++)
            {
                for (int y = 0; y < 120; y++)
                {
                    ImageArray[x, y] = botRight[x, y];
                }
            }
            for (int x = 0; x < 160; x++)
            {
                for (int y = 0; y < 120; y++)
                {
                    ImageArray[x + 160, y + 120] = topLeft[x, y];
                }
            }

            //Set the image
            SetBitmapFromArray();
            picImage.Image = bitmapImage;
        }
        private void btnPixellate_Click(object sender, EventArgs e)
        {
            //Get the size of the pixel (4 or 8)
            //Set each block of 4 or 8 to the color of the top left
            int pixelSize = (int)nudPixellate.Value;
            for (int x = 0; x < imWidth; x += pixelSize)
            {
                for (int y = 0; y < imHeight; y += pixelSize)
                {
                    Color tl = ImageArray[x, y];
                    for (int pixelX = x; pixelX < (x + pixelSize); pixelX++)
                    {
                        for (int pixelY = y; pixelY < (y + pixelSize); pixelY++)
                        {
                            ImageArray[pixelX, pixelY] = tl;
                        }
                    }
                }
            }

            //Set the image
            SetBitmapFromArray();
            picImage.Image = bitmapImage;
        }
        private void btnBlur_Click(object sender, EventArgs e)
        {
            //Set the radius of the blur (minimum 1)
            int radius = 1 + (int)nudBlurLevel.Value / 10;
            Color[,] blurredImg = new Color[320, 240];

            for (int x = 0; x < imWidth; x++)
            {
                for (int y = 0; y < imHeight; y++)
                {
                    int leftX = x - radius;
                    int rightX = x + radius;
                    int topY = y - radius;
                    int botY = y + radius;

                    int avR = 0, avG = 0, avB = 0;
                    int pixels = 0;

                    //Iterate over the surrounding rectangle and get the average color
                    for (int Px = leftX; Px <= rightX; Px++)
                    {
                        for (int Py = topY; Py <= botY; Py++)
                        {
                            if (Px < 0 || Px >= imWidth || Py < 0 || Py >= imHeight)
                            {
                                continue;
                            }
                            Color cPix = ImageArray[Px, Py];
                            if (Px == x && Py == y)
                            {
                                continue;
                            }
                            else
                            {
                                avR += 2 * cPix.R;
                                avG += 2 * cPix.G;
                                avB += 2 * cPix.B;

                                pixels += 2;
                            }

                        }
                    }
                    avR /= pixels;
                    avG /= pixels;
                    avB /= pixels;

                    Color avColor = Color.FromArgb(255, avR, avG, avB);

                    //Set the color
                    blurredImg[x, y] = avColor;
                }
            }

            //Set the image
            ImageArray = blurredImg;
            SetBitmapFromArray();
            picImage.Image = bitmapImage;
        }
        private void btnSort_Click(object sender, EventArgs e)
        {
            for (int y = 0; y < imHeight; y++)
            {
                //Sort each row individually
                Color[] rowToSort = new Color[320];
                for (int x = 0; x < imWidth; x++)
                {
                    rowToSort[x] = ImageArray[x, y];
                }

                //Now sort the array (insertion sort
                for (int i = 0; i < rowToSort.Length - 1; i++)
                {
                    for (int j = (i + 1); j > 0; j--)
                    {
                        Color prevC = rowToSort[j - 1];
                        int prevColorSum = prevC.R + prevC.G + prevC.B;
                        Color curC = rowToSort[j];
                        int ColorSum = curC.R + curC.G + curC.B;

                        if (prevColorSum > ColorSum)
                        {
                            Color temp = rowToSort[j - 1];
                            rowToSort[j - 1] = rowToSort[j];
                            rowToSort[j] = temp;
                        }
                    }
                }
                for (int x = 0; x < imWidth; x++)
                {
                    ImageArray[x, y] = rowToSort[x];
                }
            }

            //Set image
            SetBitmapFromArray();
            picImage.Image = bitmapImage;
        }
        private void btnSave_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveFile = new SaveFileDialog();
            saveFile.Filter = "PNG files (*.png)|*.png|All files (*.*)|*.*";
            if (saveFile.ShowDialog() == DialogResult.OK)
            {
                SetBitmapFromArray();
                bitmapImage.Save(saveFile.FileName);
                MessageBox.Show("Successfully saved image at:\r\n" + saveFile.FileName);
            }
        }
        private void btnSobelDetect_Click(object sender, EventArgs e)
        {
            //Blur first to avoid noise
            Gauss(picImage.Image);
            Sobel(picImage.Image);
        }

        //Scroll buttons
        private void btnLeft_Click(object sender, EventArgs e)
        {
            //Scroll by 16 each time
            int scrollPixels = 16;

            //Select the part that will move and the part that will go offscreen and wrap around
            Color[,] offScreen = new Color[scrollPixels, 240];
            int osX = 0;
            Color[,] moveToLeft = new Color[320 - scrollPixels, 240];
            int mtlX = 0;

            //Select the parts
            for (int x = 0; x < imWidth; x++)
            {
                for (int y = 0; y < imHeight; y++)
                {
                    Color curPixel = ImageArray[x, y];

                    if (x < scrollPixels)
                    {
                        offScreen[osX, y] = curPixel;
                    }
                    else
                    {
                        moveToLeft[mtlX, y] = curPixel;
                    }
                }
                if (x < scrollPixels)
                {
                    osX++;
                }
                if (x >= scrollPixels)
                {
                    mtlX++;
                }
            }
            osX = 0;
            mtlX = 0;

            //Set the parts
            for (int x = 0; x < imWidth; x++)
            {
                for (int y = 0; y < imHeight; y++)
                {
                    if (x < 320 - scrollPixels)
                    {
                        ImageArray[x, y] = moveToLeft[mtlX, y];
                    }
                    else
                    {
                        ImageArray[x, y] = offScreen[osX, y];
                    }
                }
                if (x < 320 - scrollPixels)
                {
                    mtlX++;
                }
                else
                {
                    osX++;
                }
            }

            //Set the image
            SetBitmapFromArray();
            picImage.Image = bitmapImage;
        }
        private void btnRight_Click(object sender, EventArgs e)
        {
            //Essentially same as left scroll
            int scrollPixels = 16;
            Color[,] offScreen = new Color[scrollPixels, 240];
            int osX = 0;
            Color[,] moveToRight = new Color[320 - scrollPixels, 240];
            int mtrX = 0;

            //Get the parts
            for (int x = 0; x < imWidth; x++)
            {
                for (int y = 0; y < imHeight; y++)
                {
                    Color curPixel = ImageArray[x, y];

                    if (x >= (320 - scrollPixels))
                    {
                        offScreen[osX, y] = curPixel;
                    }
                    else
                    {
                        moveToRight[mtrX, y] = curPixel;
                    }
                }
                if (x >= (320 - scrollPixels))
                {
                    osX++;
                }
                else
                {
                    mtrX++;
                }
            }
            osX = 0;
            mtrX = 0;

            //Set the parts
            for (int x = 0; x < imWidth; x++)
            {
                for (int y = 0; y < imHeight; y++)
                {
                    if (x < scrollPixels)
                    {
                        ImageArray[x, y] = offScreen[osX, y];
                    }
                    else
                    {
                        ImageArray[x, y] = moveToRight[mtrX, y];
                    }
                }
                if (x < scrollPixels)
                {
                    osX++;
                }
                else
                {
                    mtrX++;
                }
            }

            SetBitmapFromArray();
            picImage.Image = bitmapImage;
        }
        private void btnUp_Click(object sender, EventArgs e)
        {
            //Select the parts that go off and move
            int scrollPixels = 16;
            Color[,] offScreen = new Color[320, scrollPixels];
            int osY = 0;
            Color[,] moveUp = new Color[320, 240 - scrollPixels];
            int muY = 0;

            //Get the parts
            for (int y = 0; y < imHeight; y++)
            {
                for (int x = 0; x < imWidth; x++)
                {
                    Color curPixel = ImageArray[x, y];

                    if (y < scrollPixels)
                    {
                        offScreen[x, osY] = curPixel;
                    }
                    else
                    {
                        moveUp[x, muY] = curPixel;
                    }
                }
                if (y < scrollPixels)
                {
                    osY++;
                }
                else
                {
                    muY++;
                }
            }
            osY = 0;
            muY = 0;

            //Set the parts
            for (int y = 0; y < imHeight; y++)
            {
                for (int x = 0; x < imWidth; x++)
                {
                    if (y < 240 - scrollPixels)
                    {
                        ImageArray[x, y] = moveUp[x, muY];
                    }
                    else
                    {
                        ImageArray[x, y] = offScreen[x, osY];
                    }
                }
                if (y < 240 - scrollPixels)
                {
                    muY++;
                }
                else
                {
                    osY++;
                }
            }

            //Set the image
            SetBitmapFromArray();
            picImage.Image = bitmapImage;
        }
        private void btnDown_Click(object sender, EventArgs e)
        {
            //Essentially the same as up scroll
            int scrollPixels = 16;
            Color[,] offScreen = new Color[320, scrollPixels];
            int osY = 0;
            Color[,] moveDown = new Color[320, 240 - scrollPixels];
            int mdY = 0;

            //Get the parts
            for (int y = 0; y < imHeight; y++)
            {
                for (int x = 0; x < imWidth; x++)
                {
                    Color curPixel = ImageArray[x, y];

                    if (y < 240 - scrollPixels)
                    {
                        moveDown[x, mdY] = curPixel;
                    }
                    else
                    {
                        offScreen[x, osY] = curPixel;
                    }
                }
                if (y < 240 - scrollPixels)
                {
                    mdY++;
                }
                else
                {
                    osY++;
                }
            }
            osY = 0;
            mdY = 0;

            //Set the parts
            for (int y = 0; y < imHeight; y++)
            {
                for (int x = 0; x < imWidth; x++)
                {
                    if (y >= scrollPixels)
                    {
                        ImageArray[x, y] = moveDown[x, mdY];
                    }
                    else
                    {
                        ImageArray[x, y] = offScreen[x, osY];
                    }
                }
                if (y >= scrollPixels)
                {
                    mdY++;
                }
                else
                {
                    osY++;
                }
            }

            //Set the image
            SetBitmapFromArray();
            picImage.Image = bitmapImage;
        }

        private void btnHistogramStretch_Click(object sender, EventArgs e)
        {
            //Performs a full scale histogram stretch by applying a statistical approach
            // g(x,y) = (int)(255* cdf(f(x,y)))
            //where cdf is the cumulative distribution function

            int[,] fullHist = developHistogram();
            double[] histogramN = new double[256];
            for (int x = 0; x < 256; x++)
            {
                histogramN[x] = fullHist[3, x];
                histogramN[x] /= (imHeight * imWidth);
            }

            for (int x = 0; x < imWidth; x++)
            {
                for (int y = 0; y < imHeight; y++)
                {
                    int cCol = ImageArray[x, y].R;
                    double g = 0;
                    for (int n = 0; n < cCol; n++)
                    {
                        g += histogramN[n];
                    }
                    g *= 255;

                    int res = (int)g;

                    ImageArray[x, y] = Color.FromArgb(255, res, res, res);

                }
            }

            SetBitmapFromArray();
            picImage.Image = bitmapImage;
        }
        public int[,] developHistogram()
        {
            //Develops a histogram using the chart control

            //Clear the current chart
            cHistogram.Series.Clear();
            cHistogram.ChartAreas.Clear();

            //Make the right items for the chart
            cHistogram.ChartAreas.Add("caHistogram");
            cHistogram.Series.Add("Red");
            cHistogram.Series["Red"].IsVisibleInLegend = false;
            cHistogram.Series["Red"].Color = Color.FromArgb(255, 255, 0, 0);
            cHistogram.Series.Add("Green");
            cHistogram.Series["Green"].IsVisibleInLegend = false;
            cHistogram.Series["Green"].Color = Color.FromArgb(255, 0, 255, 0);
            cHistogram.Series.Add("Blue");
            cHistogram.Series["Blue"].IsVisibleInLegend = false;
            cHistogram.Series["Blue"].Color = Color.FromArgb(255, 0, 0, 255);
            cHistogram.Series.Add("Average");
            cHistogram.Series["Average"].IsVisibleInLegend = false;
            cHistogram.Series["Average"].Color = Color.FromArgb(255, 0, 0, 0);
            cHistogram.Series.Add("Blank");
            cHistogram.Series["Blank"].IsVisibleInLegend = false;

            //2d histogram array ( R G B A )
            int[,] histogram = new int[4, 256];

            for (int i = 0; i < 256; i++)
            {
                histogram[0, i] = 0;
                histogram[1, i] = 0;
                histogram[2, i] = 0;
                histogram[3, i] = 0;
            }

            for (int x = 0; x < imWidth; x++)
            {
                for (int y = 0; y < imHeight; y++)
                {
                    Color cCol = ImageArray[x, y];
                    int intensity = (int)((cCol.R + cCol.G + cCol.B) / 3);

                    histogram[0, cCol.R]++;
                    histogram[1, cCol.B]++;
                    histogram[2, cCol.G]++;
                    histogram[3, intensity]++;
                }
            }
            int maxPix = 0;

            //Set the points (also find the max)
            for (int i = 0; i < 256; i++)
            {
                cHistogram.Series["Red"].Points.AddXY(i, histogram[0, i]);
                cHistogram.Series["Green"].Points.AddXY(i, histogram[1, i]);
                cHistogram.Series["Blue"].Points.AddXY(i, histogram[2, i]);
                cHistogram.Series["Average"].Points.AddXY(i, histogram[3, i]);
                cHistogram.Series["Blank"].Points.AddXY(i, 0);
                if (histogram[0, i] > maxPix)
                {
                    maxPix = histogram[0, i];
                }
                if (histogram[1, i] > maxPix)
                {
                    maxPix = histogram[1, i];
                }
                if (histogram[2, i] > maxPix)
                {
                    maxPix = histogram[2, i];
                }
                if (histogram[3, i] > maxPix)
                {
                    maxPix = histogram[3, i];
                }


            }

            cHistogram.ChartAreas[0].Axes[1].Maximum = maxPix;
            cHistogram.ChartAreas[0].Axes[0].Minimum = 0;
            cHistogram.ChartAreas[0].Axes[0].Maximum = 255;

            //Make the cumulative chart area (uses the CDF enum)
            cHistogram.ChartAreas["caHistogram"].AxisY2.Enabled = System.Windows.Forms.DataVisualization.Charting.AxisEnabled.True;
            cHistogram.ChartAreas["caHistogram"].AxisY2.Maximum = 1;
            cHistogram.ChartAreas["caHistogram"].AxisY2.MajorGrid.Enabled = false;
            for (int i = 0; i < 4; i++)
            {
                string cSeries = Convert.ToString((CDF)i);
                cHistogram.Series.Add(cSeries);
                cHistogram.Series[cSeries].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
                cHistogram.Series[cSeries].IsVisibleInLegend = false;
                cHistogram.Series[cSeries].YAxisType = System.Windows.Forms.DataVisualization.Charting.AxisType.Secondary;
                cHistogram.Series[cSeries].BorderWidth = 2;
                cHistogram.Series[cSeries].SetCustomProperty("LineTension", "0.01");


                //Get the color right
                if (i == 0)
                {
                    cHistogram.Series[cSeries].Color = Color.DarkRed;
                }
                else if (i == 1)
                {
                    cHistogram.Series[cSeries].Color = Color.DarkGreen;
                }
                else if (i == 2)
                {
                    cHistogram.Series[cSeries].Color = Color.DarkBlue;
                }
                else if (i == 3)
                {
                    cHistogram.Series[cSeries].Color = Color.FromArgb(255, 200, 200, 200);
                }

                //Normalized histogram (pixels @ intensity / total pixels)
                double[] histogramN = new double[256];
                for (int x = 0; x < 256; x++)
                {
                    //Gets the histogram for the current color
                    histogramN[x] = histogram[i, x];
                    histogramN[x] /= (imHeight * imWidth);
                }

                for (int x = 0; x < 256; x++)
                {
                    double cPoint = 0;
                    for (int j = 0; j < x; j++)
                    {
                        cPoint += histogramN[j];
                    }
                    cHistogram.Series[cSeries].Points.AddXY(x, cPoint);
                }
            }

            //Make the appropriate ones visible
            if (cbRed.Checked)
            {
                cHistogram.Series["Red"].Enabled = true;
                cHistogram.Series["RedCDF"].Enabled = true;
            }
            else
            {
                cHistogram.Series["Red"].Enabled = false;
                cHistogram.Series["RedCDF"].Enabled = false;
            }
            if (cbBlue.Checked)
            {
                cHistogram.Series["Blue"].Enabled = true;
                cHistogram.Series["BlueCDF"].Enabled = true;
            }
            else
            {
                cHistogram.Series["Blue"].Enabled = false;
                cHistogram.Series["BlueCDF"].Enabled = false;
            }
            if (cbGreen.Checked)
            {
                cHistogram.Series["Green"].Enabled = true;
                cHistogram.Series["GreenCDF"].Enabled = true;
            }
            else
            {
                cHistogram.Series["Green"].Enabled = false;
                cHistogram.Series["GreenCDF"].Enabled = false;
            }
            if (cbAverage.Checked)
            {
                cHistogram.Series["Average"].Enabled = true;
                cHistogram.Series["AverageCDF"].Enabled = true;
            }
            else
            {
                cHistogram.Series["Average"].Enabled = false;
                cHistogram.Series["AverageCDF"].Enabled = false;
            }


            System.Windows.Forms.DataVisualization.Charting.Title t = new System.Windows.Forms.DataVisualization.Charting.Title();
            t.Text = ("Histogram");

            if (cHistogram.Titles.Count >= 1)
                cHistogram.Titles[0] = t;
            else
                cHistogram.Titles.Add(t);

            return histogram;
        }
        private void cbExtraSharpen_CheckedChanged(object sender, EventArgs e)
        {
            //Extra sharpen means that max gradients will be turned up by a factor of 8, not good for noisy images
            if (cbExtraSharpen.Checked)
            {
                extraSharpen = true;
            }
            else
            {
                extraSharpen = false;
            }
        }

        private void btnThreshold_Click(object sender, EventArgs e)
        {
            //Binarize the image based on the thresholding value
            int thresh = (int)nudThreshold.Value;
            btnGrayscale.PerformClick();

            for (int x = 0; x < imWidth; x++)
            {
                for (int y = 0; y < imHeight; y++)
                {
                    if (ImageArray[x, y].R > thresh)
                    {
                        ImageArray[x, y] = Color.FromArgb(255, 255, 255, 255);
                    }
                    else
                    {
                        ImageArray[x, y] = Color.FromArgb(255, 0, 0, 0);
                    }

                }
            }
            SetBitmapFromArray();
            picImage.Image = bitmapImage;
        }

        private void btnDevelop_Click(object sender, EventArgs e)
        {
            developHistogram();
        }

        private void rbRed_CheckedChanged(object sender, EventArgs e)
        {
            developHistogram();
        }

        private void rbGreen_CheckedChanged(object sender, EventArgs e)
        {
            developHistogram();
        }

        private void rbBlue_CheckedChanged(object sender, EventArgs e)
        {
            developHistogram();
        }

        private void rbIntensity_CheckedChanged(object sender, EventArgs e)
        {
            developHistogram();
        }

        private void cbRed_CheckedChanged(object sender, EventArgs e)
        {
            developHistogram();
        }

        private void cbBlue_CheckedChanged(object sender, EventArgs e)
        {
            developHistogram();
        }

        private void cbGreen_CheckedChanged(object sender, EventArgs e)
        {
            developHistogram();
        }

        private void cbAverage_CheckedChanged(object sender, EventArgs e)
        {
            developHistogram();
        }

        private void btnHelp_Click(object sender, EventArgs e)
        {
            //Open up the help text file
            Process.Start("edgeHelp.txt");
        }

        private void btnRegionCount_Click(object sender, EventArgs e)
        {
            btnGrayscale.PerformClick();
        }
        public int binVal(Color c)
        {
            int v = ((c.R + c.G + c.B) / 3);
            if(v > 125)
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }
    }
}
